#!/usr/bin/env python3
# coding=utf-8

"""
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF 2018"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"

import sys
import struct
import socket
import atexit
import time
# import typing
import enum
# import math
# import copy
import gc

import numpy as np


# Ensure thread switching with high time resolution. (System default
# may be as high as 5e-3)
sys.setswitchinterval(1e-5)

np.set_printoptions(precision=5, suppress=True)


class PST(enum.IntEnum):
    """Packet state and type constants"""
    NONE = 0
    CLIENT = 1
    SERVER = 2
    REJECTED = 3
    QUIT = 4
    INIT = 5
    ACK = 6
    CONN = 7


class Packet:
    """Data packet format between client and server"""

    # Cycle ID, State, Type, Actual, Commanded
    struct = struct.Struct('<BBB7d7d16d')

    def __init__(self, *args):
        if len(args) == 0:
            self.data = (17 + 16)*[0]
        elif len(args) == 1 and type(args[0]) == bytes:
            self.bytes = args[0]
        elif len(args) == (17 + 16):
            self.data = args
        else:
            raise Exception("_Packet.__init__: Need 0, 1 or 17 arguments for initialization.")

    def __repr__(self):
        return 'cid:{o.cycle_id} state:{o.state} type:{o.type}'.format(o=self)

    def get_data(self):
        return ([self.cycle_id, self.state, self.type] +
                list(self.joint_positions_act) +
                list(self.joint_positions_cmd))

    def set_data(self, data):
        self.cycle_id = data[0]
        self.state = data[1]
        self.type = data[2]
        self.joint_positions_act = list(data[3:3+7])
        self.joint_positions_cmd = list(data[3+7:])

    data = property(get_data, set_data)

    def get_bytes(self):
        return Packet.struct.pack(*self.data)

    def set_bytes(self, bytes):
        data = Packet.struct.unpack(bytes)
        self.data = data

    bytes = property(get_bytes, set_bytes)


# Storage for logging data points
N = 0
cmd = np.empty((N, 8), dtype=np.float64)
act = np.empty((N, 8), dtype=np.float64)
act_d = np.empty((N, 8), dtype=np.float64)


def plot():
    import matplotlib.pyplot as plt
    max_i = min(N, cc)
    mj = mjs[0]
    mji = mj+1
    # Positions
    plt.subplot(3,1,1)
    plt.plot(cmd[:max_i, 0], cmd[:max_i, mji], marker='o', label='$p_{cmd}$', color='blue')
    plt.plot(act[:max_i, 0], act[:max_i, mji], marker='*', label='$p_{act}$', color='red')
    plt.plot(act_d[:max_i, 0], act_d[:max_i, mji], marker='x', label='$p_{act\_d}$', color='green')
    plt.legend()
    plt.grid()

    # Time errors
    plt.subplot(3,1,2)
    plt.plot(cmd[1:max_i, 0], np.diff(cmd[:max_i, 0]), marker='o', label='$\delta t$', color='blue')
    plt.legend()
    plt.grid()

    # Velocities
    plt.subplot(3,1,3)
    plt.plot(cmd[1:max_i, 0], np.diff(cmd[:max_i, mji], axis=0)/0.001, marker='o', label='$v_{cmd}$', color='blue')
    plt.plot(act[1:max_i, 0], np.diff(act[:max_i, mji], axis=0)/0.001, marker='x', label='$v_{act}$', color='red')
    plt.legend()
    plt.grid()
    plt.show()
    
# Joint index to move
mj = 3
#mjs = [4, 6] #[mj]
# Amplitude
a = np.zeros(7, dtype=np.float64)
a[mj] = 0.01 * np.pi
# Angular velocity
w = np.random.choice((-1.0,1.0)) * np.zeros(7, dtype=np.float64)
w[mj] = 2 * np.pi
print('A={:.2f} w={:.2f} v_max={:.2f} a_max={:.2f}'
      .format(a[mj], w[mj], a[mj]*w[mj], a[mj]*w[mj]**2))



# class BytePacket(Packet):
    

fms_addr = ('127.0.0.1', 9999)
s = socket.socket(type=socket.SOCK_DGRAM)


def quit():
    print('Sending QUIT')
    s.sendto(quitp.bytes, fms_addr)

atexit.register(quit)

t0 = None # time.time()
t = 0
cc = 0
cycle_time = 0.01
cycle_filter_alpha = 0.1
dt_init = 0.3

initp = Packet()
initp.type = PST.INIT
quitp = Packet()
quitp.type = PST.QUIT
cmdp = Packet()
actp = Packet()
ackp = Packet()
cmdp.type = PST.CLIENT

gc.disable()
gc.collect()

s.sendto(initp.bytes, fms_addr)
ackp.bytes = s.recv(4096)
if ackp.type == PST.ACK:
    print('ack packet: ', ackp)
    print('initial jps:', ackp.joint_positions_act)
elif ackp.type == PST.REJECTED:
    print('rejected')
    
cmdp.joint_positions_cmd = ackp.joint_positions_act
# print('client initial jps:', cmdp.joint_positions_cmd)
# fci_packets = []
j_init = np.array(ackp.joint_positions_act)
try:
    while True:
        actp.bytes = s.recv(4096)
        if t0 is None:
            cid0 = cid = actp.cycle_id
            cid_last = (cid - 1) % 256
            t0 = time.time()
            t = 0.0
            t_pll = 0.0
            t_last = t-cycle_time
        else:
            cid_last = cid
            cid = actp.cycle_id
            t_last = t
            t = time.time() - t0
            dcid = (cid - cid_last) % 256
            cycle_time = cycle_filter_alpha * (t-t_last) / dcid +  (1 - cycle_filter_alpha) * cycle_time
            if dcid != 1:
                print('Warning: Cycle skip dcid={}'.format(dcid))
            t_pll += dcid * cycle_time
            t_err = t - t_pll
            if t_err > 0.005:
                print(cid, t_err)
        # Rest for 0.1 s before starting the cosine
        if t < dt_init:
            cmdp.joint_positions_cmd = j_init
        else:
            cmdp.joint_positions_cmd = j_init + a * (1 - np.cos(w * (t_pll - dt_init)))
        s.sendto(cmdp.bytes, fms_addr)
        # Store trajectories
        if cc < N:
            act[cc, 0] = t_pll
            act[cc, 1:] = actp.joint_positions_act
            act_d[cc, 0] = t_pll
            act_d[cc, 1:] = actp.joint_positions_cmd
            cmd[cc, 0] = t_pll
            cmd[cc, 1:] = cmdp.joint_positions_cmd
            cc += 1
        # print(actp.cycle_id)
        # Time for garbage collection
        gc.collect()
except Exception as e:
    print('Exception in main loop: ', e)
    quit()
except KeyboardInterrupt:
    print('User aborted execution')
    quit()

gc.enable()

