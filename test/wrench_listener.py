# coding=utf-8

"""
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF 2018"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"

import struct
import socket

import numpy as np
np.set_printoptions(precision=2, suppress=True)

w_struct = struct.Struct('6d')

waddr = ('0.0.0.0', 10000)
ws = socket.socket(type=socket.SOCK_DGRAM)
ws.bind(waddr)

w=np.zeros(6, dtype=np.float64)
alpha = 1.0
while True:
    w = (1-alpha) * w + alpha * np.array(w_struct.unpack(ws.recv(1024)))
    print(w)
