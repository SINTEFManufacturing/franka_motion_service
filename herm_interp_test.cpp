// Copyright: SINTEF 2018
// License: GPLv3
// Created by: Morten Lind
// eaddress: morten.lind@sintef.no

#include "hermite_interpolator.hpp"
#include <valarray>
#include <numeric>
#include <iostream>
#include <fstream>
#include <iterator>

using namespace std;


template <class T>
ostream& operator<<(ostream& o, const valarray<T>& varr)
{
    copy(begin(varr), end(varr), ostream_iterator<T>(o, " "));
    return o;
}


int main(int argc, char** argv) {
    valarray<double> p0={0,0}, p1={1,1}, v0={1,0}, v1={0,1};
    CubicHermite ch(0, 1, p0, p1, v0, v1, false); 
    // cout << ch(0.5) << endl;
    valarray<double> ts(15); // = {-0.1, 0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1};
    iota(begin(ts), end(ts), -2);
    ts /= 10.0;
    cout << ts << endl;
    valarray<valarray<double>> ps = ch(ts);
    ofstream tout("traj.data");
    uint16_t i;
    for(i=0; i<ps.size(); i++) {  //const auto& p : ch(ts)){
        cout << ts[i] << " " << ps[i] << endl;
        tout << ts[i] << " " << ps[i] << endl;
    }
    // Durability test
    double t;
    valarray<double> pos;
    for(t=0.0; t<1.0 ; t+=1e-6){
        pos = ch(t);
        cout << "@" << t << " : " << pos << endl;
    }
    return 0;
}
