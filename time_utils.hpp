#pragma once

// Copyright: SINTEF 2018
// License: GPLv3
// Created by: Morten Lind
// eaddress: morten.lind@sintef.no

#include <chrono>


extern chrono::time_point<chrono::high_resolution_clock> start_tp;

// Epoch timestamp
double ts(){
    return 1.0e-9 * chrono::high_resolution_clock::now().time_since_epoch().count();
}


// Process timestamp
double pts(){
    return 1.0e-9 * chrono::duration_cast<chrono::nanoseconds>(chrono::high_resolution_clock::now() - start_tp).count();
}


chrono::time_point<chrono::high_resolution_clock> tp_from_ts(double ts){
    return chrono::time_point<chrono::high_resolution_clock>(chrono::nanoseconds( 1000000000 * (long long)(floor(ts)) + (long long)(1.0e9 * (ts - floor(ts)))));
}


double ts_from_tp(chrono::time_point<chrono::high_resolution_clock> tp){
    return 1e-9 * tp.time_since_epoch().count();
}


double tp_diff(chrono::time_point<chrono::high_resolution_clock> tp0,
                  chrono::time_point<chrono::high_resolution_clock> tp1) {
    return 1e-9 * chrono::duration_cast<chrono::nanoseconds>(tp1 - tp0).count();
}
