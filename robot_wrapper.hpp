#pragma once

// Copyright: SINTEF 2018
// License: GPLv3
// Created by: Morten Lind
// eaddress: morten.lind@sintef.no

#include <boost/asio.hpp>
#include <franka/robot_state.h>
#include <franka/robot.h>
#include <franka/duration.h>


using namespace std;

class RobWrap {
private:
    franka::Robot *phys_rob = nullptr;
    RobotEmulator *virt_rob = nullptr;
    bool initialized = false;
public:
    void init_phys(char *fci_host){
        // Initialize for using the physical robot, i.e. FCI
        cout << "Initializing the physical robot at host \"" << fci_host << "\"."<< endl;
        phys_rob = new franka::Robot(fci_host);
        phys_rob->automaticErrorRecovery();
	std::array<double,7> jimp;
	jimp.fill(10000.0);
	phys_rob->setJointImpedance(jimp);
        // From example
        // phys_rob->setCollisionBehavior(
        //                                  {{20.0, 20.0, 18.0, 18.0, 16.0, 14.0, 12.0}}, {{20.0, 20.0, 18.0, 18.0, 16.0, 14.0, 12.0}},
        //                                  {{20.0, 20.0, 18.0, 18.0, 16.0, 14.0, 12.0}}, {{20.0, 20.0, 18.0, 18.0, 16.0, 14.0, 12.0}},
        //                                  {{20.0, 20.0, 20.0, 25.0, 25.0, 25.0}}, {{20.0, 20.0, 20.0, 25.0, 25.0, 25.0}},
        //                                  {{20.0, 20.0, 20.0, 25.0, 25.0, 25.0}}, {{20.0, 20.0, 20.0, 25.0, 25.0, 25.0}});
        double coll_upper = 1000.0;
        double coll_lower = 1000.0;
        phys_rob->setCollisionBehavior(
                      {{coll_lower, coll_lower, coll_lower, coll_lower, coll_lower, coll_lower, coll_lower}},
                      {{coll_upper, coll_upper, coll_upper, coll_upper, coll_upper, coll_upper, coll_upper}},
                      {{coll_lower, coll_lower, coll_lower, coll_lower, coll_lower, coll_lower, coll_lower}},
                      {{coll_upper, coll_upper, coll_upper, coll_upper, coll_upper, coll_upper, coll_upper}},
                      {{coll_lower, coll_lower, coll_lower, coll_lower, coll_lower, coll_lower}},
                      {{coll_upper, coll_upper, coll_upper, coll_upper, coll_upper, coll_upper}},
                      {{coll_lower, coll_lower, coll_lower, coll_lower, coll_lower, coll_lower}},
                      {{coll_upper, coll_upper, coll_upper, coll_upper, coll_upper, coll_upper}});
    
	initialized = true;
    }
    void init_virt(boost::asio::io_service& io_service, uint32_t mic_cycle_time_us){
        // Initialize for using the virtual robot.
        cout << "Initializing the robot emulator" << endl;
        virt_rob = new RobotEmulator(io_service, mic_cycle_time_us);
        initialized = true;
    }
    void control(std::function<franka::JointPositions(const franka::RobotState&, franka::Duration)> motion_generator_callback){
        if (phys_rob != nullptr){
	  phys_rob -> control(motion_generator_callback, franka::ControllerMode::kJointImpedance, true, 10.0);
        }
        else if (virt_rob != nullptr){
            virt_rob -> control(motion_generator_callback);
        }
        else{
            throw std::runtime_error("RobWrap not initialized!");
        }
    }
    franka::RobotState readOnce(){
        if (phys_rob != nullptr){
            return phys_rob -> readOnce();
        }
        else if (virt_rob != nullptr){
            return virt_rob -> readOnce();
        }
        else{
            throw std::runtime_error("RobWrap not initialized!");
        }
    }
};

