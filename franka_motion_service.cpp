// Copyright: SINTEF 2018
// License: GPLv3
// Created by: Morten Lind
// eaddress: morten.lind@sintef.no

#include <valarray>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <chrono>
#include <thread>
#include <sched.h>
#include <pthread.h>
#include <boost/asio.hpp>
#include <iomanip>
#include <mutex>              // std::mutex, std::unique_lock
#include <condition_variable> // std::condition_variable
//#include <stdint.h>
#include <unistd.h>

// From https://rt.wiki.kernel.org/index.php/Simple_memory_locking_example
#include <sys/mman.h> // Needed for mlockall()
       

// #include <franka/robot.h>
#include <franka/robot_state.h>
#include <franka/control_types.h>
#include <franka/duration.h>
#include <franka/rate_limiting.h>

#include "hermite_interpolator.hpp"
#include "time_utils.hpp"
#include "robot_emulator.hpp"
#include "robot_wrapper.hpp"

using boost::asio::ip::udp;
using namespace std;


// HRT time point for global use
chrono::time_point<chrono::high_resolution_clock> start_tp;


// Output stream for std::array
template <class T, std::size_t N>
ostream& operator<<(ostream& o, const array<T, N>& arr)
{
  copy(arr.cbegin(), arr.cend(), ostream_iterator<T>(o, " "));
  return o;
}


// Output stream for std::valarray
template <class T>
ostream& operator<<(ostream& o, const valarray<T>& varr)
{
  if (o.flags() & ios::binary) {
    copy(begin(varr), end(varr), ostream_iterator<T>(o));
  }
  else {
    copy(begin(varr), end(varr), ostream_iterator<T>(o, " "));
  }
  return o;
}


// Constant for UDP packet size
enum { max_length = 4096 };


// Packet state and type constants
enum CC {
	 NONE=0,
	 CLIENT=1,
	 SERVER=2,
	 REJECTED=3,
	 QUIT=4,
	 INIT=5,
	 ACK=6,
	 CONN=7,
};


// The packet struct for communicating with the external TG
struct Packet{
  uint8_t cycle;
  uint8_t state;
  uint8_t type;
  double joint_positions_act[7];
  double joint_positions_cmd[7];
  double flange_pose_act[16];
} __attribute__((__packed__));
ostream& operator << (std::ostream &o, const Packet &a){
  return o << "cid:" << unsigned(a.cycle) << " state:" << unsigned(a.state) << " type:" << unsigned(a.type);
}


// // Frankas method for limiting the velocity
// std::array<double, 7> limitRate(const std::array<double, 7>& max_derivatives,
//                                 const std::array<double, 7>& desired_values,
//                                 const std::array<double, 7>& last_desired_values) {
//   std::array<double, 7> limited_values{};
//   for (size_t i = 0; i < 7; i++) {
//     double desired_difference = (desired_values[i] - last_desired_values[i]) / mic_cycle_time;
//     limited_values[i] =
//       last_desired_values[i] +
//       std::max(std::min(desired_difference, max_derivatives[i]), -max_derivatives[i]) * mic_cycle_time;
//   }
//   return limited_values;
// }


void valarr_into_arr(const std::valarray<double>& vala, std::array<double,7>& arr){
  std::copy(begin(vala), end(vala), begin(arr));
  return;
}

bool valarr_clip(std::valarray<double>& vala, double abs_max){
  bool excess_flag = false;
  for(auto i=0; i<vala.size(); i++){
    if(vala[i] > abs_max){
      excess_flag = true;
      vala[i] = abs_max;
    }
    if(vala[i] < -abs_max){
      excess_flag = true;
      vala[i] = -abs_max;
    }
  }
  return excess_flag;
}


class FMS {
private:
  boost::asio::io_service& io_service;
  uint16_t port, wport;
  udp::socket *fms_sock, *wrench_sock;
  udp::endpoint connection;
  bool has_connection;
  udp::endpoint base_connection, wrench_connection, wrench_bc_ep;
  uint32_t mic_cycle;
  uint64_t mac_cycle;
  const uint8_t mics_per_mac; // Micro cycles per macro cycle
  const double mic_cycle_time, mac_cycle_time, mac_cycle_time_sq;
  Packet client_packet, mac_state_packet;
  bool new_client_packet;
  bool new_connection;
  franka::RobotState fms_state, hot_fci_state, initial_rs;
  franka::Duration hot_time_step;
  condition_variable hot_cond;
  mutex hot_mtx;
  array<double,7> acur_pos, aprev_pos,
    acur_vel, aprev_vel,
    acur_acc, aprev_acc,
    acur_jerk, aprev_jerk;
  valarray<double> new_tpos, la_tpos, cur_tpos, prev_tpos,
    cur_tvel, prev_tvel,
    cur_tacc, prev_tacc,
    cur_tjerk, prev_tjerk,
    delta_pos, delta_pos_act,
    new_pos, cur_pos, prev_pos,
    cur_vel, prev_vel,
    cur_acc, prev_acc,
    cur_jerk, prev_jerk;
  // Time point and stamp variables
  double cur_ts, prev_ts, start_ts, now_ts;
  chrono::time_point<chrono::high_resolution_clock> cur_tp, prev_tp, now_tp;
  // A counter for number of missed packet deadlines in a row
  uint8_t missed_count;
  // Flag for a macro cycle without new client target
  double alpha;
  double w_alpha;
  valarray<double> wrench;
  // Debugging output flags
  bool debug_conn=true, debug_mac=false, debug_mic=false, debug_traj=true, debug_time=false;
  ofstream fccout, fcaout, cliout;
  bool quintic;
  CubicHermite cinterp;
  QuinticHermite qinterp;
  double calc_t_max = 0.0;
  double max_spd = 1.0; // radian per second
  double max_acc = 15.0; // radian per square second
  double max_jerk = 10000.0; // radian per cubic second
  bool limit_jerk_flag = true;
  bool emu_flag;
  
public:
  FMS(boost::asio::io_service& io_service, char *tg_network, uint16_t port, double mic_cycle_time, uint8_t mics_per_mac, franka::RobotState initial_rs, bool emu=false):
    port(port),
    io_service(io_service),
    base_connection(udp::v4(), port),
    has_connection(false),
    mics_per_mac(mics_per_mac),
    mic_cycle(mics_per_mac-1),  // Starting with a mic cycle at the end of a mac cycle ensures that the controller starts with a new mac cycle
    mac_cycle(-1),
    mic_cycle_time(mic_cycle_time),
    mac_cycle_time(mic_cycle_time * mics_per_mac),
    mac_cycle_time_sq(mac_cycle_time*mac_cycle_time),
    initial_rs(initial_rs),
    new_client_packet(false),
    new_connection(false),
    cur_tpos(7), prev_tpos(7),
    cur_tjerk(7), prev_tjerk(7),
    cur_pos(7), prev_pos(7),
    cur_vel(7), prev_vel(7),
    cur_acc(7), prev_acc(7),
    cur_jerk(7), prev_jerk(7),
    delta_pos(7),
    missed_count(0),
    alpha(0.1),
    fccout("fci_cmd_traj.npy", ios::binary),
    fcaout("fci_act_traj.npy", ios::binary),
    cliout("cli_traj.npy", ios::binary),
    quintic(true), qinterp(), cinterp(),
    w_alpha(0.1),
    wrench(0.0, 6),
    emu_flag(emu)
    //connection(boost::asio::ip::address_v4::any(), port),
    //fms_sock(io_service, connection)
  {
    // Initialize mac data structures
    la_tpos = cur_tpos = prev_tpos = valarray<double>(initial_rs.q_d.data(), 7);
    cur_tvel = prev_tvel = {0,0,0,0,0,0,0};
    cur_tacc = prev_tacc = {0,0,0,0,0,0,0};
    cur_tjerk = prev_tjerk = {0,0,0,0,0,0,0};
    // Initialize mic data structures
    cur_pos = prev_pos = valarray<double>(initial_rs.q_d.data(), 7);
    cur_vel = prev_vel = {0,0,0,0,0,0,0};
    cur_acc = prev_acc = {0,0,0,0,0,0,0};
    cur_jerk = prev_jerk = {0,0,0,0,0,0,0};
    start_ts = cur_ts = pts();
    start_tp = cur_tp = chrono::high_resolution_clock::now();
    prev_ts = cur_ts -  mac_cycle_time;
    prev_tp = cur_tp - chrono::milliseconds(int(1000*mic_cycle_time));
    delta_pos = delta_pos_act = {0,0,0,0,0,0,0};
    cout << "initial : " << cur_pos << endl;
    connection = base_connection;
    fms_sock = new udp::socket(io_service, base_connection);
    //fms_sock->set_option(boost::asio::socket_base::broadcast(true));
    // fms_sock->non_blocking(true);
    wrench_sock = new udp::socket(io_service, wrench_connection);
    wrench_sock->set_option(udp::socket::reuse_address(true));
    wrench_sock->set_option(boost::asio::socket_base::broadcast(true));
    if(!emu_flag){
      wrench_bc_ep = udp::endpoint(boost::asio::ip::address_v4::broadcast(boost::asio::ip::address_v4::address_v4::from_string(tg_network), boost::asio::ip::address_v4::address_v4::from_string("255.255.255.0")), port+1);
    }
    cout << connection << endl;
    cout << wrench_bc_ep << endl;
    cout << "Macro cycle time = " << mac_cycle_time << " s" << endl;
  }

  void _send_macc_state() {
    // Send a macro cycle state to the external TG.
    if(has_connection) {
      // if(debug) cout<<"Sending FMS Packet to "<<connection<<endl;
      // ostringstream ostr;
      // ostr << fms_state;
      // string str(ostr.str());
      // fms_sock->send_to(boost::asio::buffer(str.c_str(), str.length()), connection);
      mac_state_packet.cycle = mac_cycle;
      mac_state_packet.state = CONN;
      mac_state_packet.type = SERVER;
      copy(begin(fms_state.q), end(fms_state.q), mac_state_packet.joint_positions_act);
      copy(begin(fms_state.q_d), end(fms_state.q_d), mac_state_packet.joint_positions_cmd);
      copy(begin(fms_state.O_T_EE), end(fms_state.O_T_EE), mac_state_packet.flange_pose_act);
      fms_sock->send_to(boost::asio::buffer((char *)&mac_state_packet, sizeof(Packet)), connection);
      // if (debug_traj){ cout<<"Sent mac state. Joint positions: " << fms_state.q << endl;}
    }
  }

  void _send_wrench() {
    // Send the wrench on the wrench socket.
    array<double,6> awrench;
    copy(begin(wrench), end(wrench), begin(awrench));
    if(debug_mac) cout << "Sending wrench" << endl;
    try{
      wrench_sock->send_to(boost::asio::buffer(awrench, 6*sizeof(double)), wrench_bc_ep);
    }
    catch (std::exception& e) {
      std::cerr << "Exception during sending wrench: " << e.what() << "\n";
    }
    if(debug_mac) cout << "Wrench sent" << endl;
  }

  void intp_update_tracking() {
  // Update the interpolator based on a client target.
    //if (debug_traj) cout << "Client interp" << endl;
    // prev_prev_tpos = prev_tpos;
    prev_tpos = cur_tpos;
    cur_tpos = la_tpos;
    la_tpos = new_tpos;
    
    // prev_prev_tvel = prev_tvel;
    prev_tvel = cur_tvel;
    cur_tvel = 0.5 * ((la_tpos - cur_tpos) + (cur_tpos - prev_tpos)) / mac_cycle_time;
    
    // prev_prev_tacc = prev_tacc;
    prev_tacc = cur_tacc;
    cur_tacc = (cur_tvel - prev_tvel) / mac_cycle_time;

    prev_tjerk = cur_tjerk;
    cur_tjerk = (cur_tacc - prev_tacc) / mac_cycle_time;
  }
  
  void intp_client_target() {
    intp_update_tracking();
    try{
      if(quintic){
	qinterp.setup(0.0, mac_cycle_time, prev_tpos, cur_tpos, prev_tvel, cur_tvel, prev_tacc, cur_tacc, false);
      }
      else{
	cinterp.setup(0.0, mac_cycle_time, prev_tpos, cur_tpos, prev_tvel, cur_tvel, false);
      }
    }
    catch (std::exception& e) {
      std::cerr << "Exception: : intp_client_target : " << e.what() << "\n";
    }
  }
    
  void intp_brake(bool full=true) {
    // Calculate a new, internally generated target pose by damping the expected acceleration and add some braking.
    // prev_prev_tpos = prev_tpos;
    // prev_tpos = cur_tpos; // Control from assumed reached target
    // // prev_tpos = valarray<double>(fms_state.q.data(), 7); // Control from actual current joint position
    // prev_prev_tvel = prev_tvel;
    // prev_tvel = cur_tvel;
    // prev_prev_tacc = prev_tacc;
    // prev_tacc = cur_tacc;
    // Apply braking and damping of current acceleration
    // if (full) {
    //   cur_tvel *= 0.7;
    // }
    // else {
    //   cur_tacc *= 0.7;
    //   cur_tvel *= 0.3;
    //   cur_tvel += 0.7 * cur_tacc;
    // }
    // Calculate extrapolated target pose by damping velocity and update tracking
    new_tpos = la_tpos + 0.5 * cur_tvel * mac_cycle_time;
    intp_update_tracking();
    // Update the interpolator.
    // interp.setup(tp_diff(start_tp, prev_tp), tp_diff(start_tp, cur_tp), prev_tpos, cur_tpos, prev_tvel, cur_tvel, false);
    try{
      if(quintic){
	qinterp.setup(0.0, mac_cycle_time, prev_tpos, cur_tpos, prev_tvel, cur_tvel, prev_tacc, cur_tacc, false);
      }
      else{
	cinterp.setup(0.0, mac_cycle_time, prev_tpos, cur_tpos, prev_tvel, cur_tvel, false);
      }
    }
    catch (std::exception& e) {
      std::cerr << "Exception: : intp_brake : " << e.what() << "\n";
    }
  }

  void fci_response(RobWrap& robot){
    cout << "FCI-Control Starting, using ";
    if (quintic){
      cout << "quintic";
    }
    else{
      cout << "cubic";
    }    
    cout << " Hermite splines"  << endl;
    try{
      robot.control([this](const franka::RobotState& rs, franka::Duration t) -> franka::JointPositions {
		      return this->fci_update(rs, t);
		    });
    }
    catch (std::exception& e) {
      std::cerr << "Exception: robot.control : " << endl << e.what() << endl;
      fccout.flush();
      fcaout.flush();
      cliout.flush();               
    }
    cout << "FCI-Control Ended!!!" << endl;
  }

  franka::JointPositions fci_update(const franka::RobotState& recv_fci_state,
                                    franka::Duration recv_time_step){
    // Instant response to the controller
    hot_fci_state = recv_fci_state;
    hot_time_step = recv_time_step;
    // Announce hot data
    // unique_lock<mutex> lck(hot_mtx);
    // If debugging the trajectory write the actual and commanded positions.
    if (has_connection && debug_traj) {
      double tmp = tp_diff(start_tp, chrono::high_resolution_clock::now());
      fcaout.write((char*)&tmp, sizeof(double));
      fcaout.write((char*)&(hot_fci_state.q[4]), sizeof(double));
      //fccout.write((char*)&tmp, sizeof(double));
      //fccout.write((char*)&(cur_pos[4]), sizeof(double));
    }
    valarr_into_arr(cur_pos, acur_pos);
    // Notify for computation of next micro cycle
    hot_cond.notify_all();
    return franka::JointPositions(acur_pos);
  }

  void update_micro_tracking(bool limit_rates){
    prev_pos = cur_pos;
    cur_pos = new_pos;
    
    prev_vel = cur_vel;
    cur_vel = (cur_pos - prev_pos) / mic_cycle_time;
    if(limit_rates) valarr_clip(cur_vel, max_spd);
    
    prev_acc = cur_acc;
    cur_acc = (cur_vel - prev_vel) / mic_cycle_time;
    if(limit_rates) valarr_clip(cur_acc, max_acc);
    
    prev_jerk = cur_jerk;
    cur_jerk = (cur_acc - prev_acc) / mic_cycle_time;
    if(limit_rates) valarr_clip(cur_jerk, max_jerk);
    
    if(limit_rates) {// Reverse update
      cur_acc = prev_acc + cur_jerk * mic_cycle_time;
      cur_vel = prev_vel + cur_acc * mic_cycle_time;
      cur_pos = prev_pos + cur_vel * mic_cycle_time;
    }
  }
  
  void fci_interp_rt(){
    // Real time based calculation
    bool missed_client_deadline;
    unique_lock<mutex> lck(hot_mtx);
    for(;;){
      // Start by waiting for announced arrival of hot data.
      hot_cond.wait(lck);
      now_ts = pts();
      now_tp = chrono::high_resolution_clock::now();
      // Store the fms state
      fms_state = hot_fci_state;
      // Update wrench
      wrench = (1.0-w_alpha) * wrench + w_alpha * valarray<double>(fms_state.K_F_ext_hat_K.data(),6);
      if(now_ts > cur_ts){
	// New macro cycle
	mac_cycle++;
	// Update interpolator times
	// Update time stamps
	// prev_prev_ts = prev_ts;
	prev_ts = cur_ts;
	cur_ts += mac_cycle_time;
	// prev_prev_tp = prev_tp;
	prev_tp = cur_tp;
	cur_tp = now_tp + chrono::microseconds((unsigned long)(mac_cycle_time*1e6));
	//interp_start_ts = interp_end_ts;
	//interp_end_ts = now_ts + mac_cycle_time;
	if(!emu_flag){
	  _send_wrench();
	}
	// Check if an external TG is connected
	if (has_connection) {
	  try {
	    // Indicator for missed deadline
	    missed_client_deadline = ! new_connection && ! new_client_packet;
	    // Indicator for first macro cycle for a new connection
	    bool init_macc = new_connection;
	    // Clear flags for new packet and new connection.
	    new_client_packet = false;
	    new_connection = false;
	    // Send the macro state to the external TG.
	    _send_macc_state();
	    if (init_macc) {
	      // Do not act upon a new connection, but continue full braking
	      missed_count = 0;
	      intp_brake(true);
	    }
	    else if (missed_client_deadline) {
	      // We were supposed to have received a packet, but
	      // none was received. Missed deadline or
	      // disconnect. Generate interpolation segment with
	      // mild braking and some attenuation of the
	      // current acceleration.
	      cout << "!!! Warning !!! : no new client packet for control" << endl;
	      missed_count++;
	      if (missed_count > 10){
		cout << "!!! Disconnecting client (missed_count=" << unsigned(missed_count) << ")" << endl;
		connection = base_connection;
		has_connection = false;
		intp_brake(true);
	      }
	      else {
		// Apply mild braking
		intp_brake(false);
	      }
	    } // if missed
	    else { // Regular situation with connection, and a client shipped a new target pose
	      // Clear missed count
	      missed_count = 0;
	      // Start the interpolation from the newly commanded joint position.
	      new_tpos = valarray<double>(client_packet.joint_positions_cmd, 7);
	      intp_client_target();
	      if (debug_traj) {
		//tmp = tp_diff(start_tp, now_tp); // + mac_cycle_time;
		cliout.write((char*)&now_ts, sizeof(double));
		//tmp = new_tpos[4];
		cliout.write((char*)&la_tpos[4], sizeof(double));
		cliout.write((char*)&cur_tvel[4], sizeof(double));
		cliout.write((char*)&cur_tacc[4], sizeof(double));
		cliout.flush();
	      }
	    }
	  }
	  catch (std::exception& e) {
	    cout << "Exception : has_connection : " << e.what() << endl;
	  }
	} // end has_connection
	else {
	  // No connection. Generate full braking.
	  if (debug_mic) cout << "No connection, full braking" << endl;
	  intp_brake(true);
	}
      } // if new_macc
      // Get a new interpolated micro position
      try {
	//cur_pos = interp(tp_diff(start_tp, now_tp));
	if(quintic){
	  new_pos = qinterp(now_ts - prev_ts);
	}
	else{
	  new_pos = cinterp(now_ts - prev_ts);
	}
      }
      catch (const exception &exc) {
	cout << "Exception when calling interpolator with time (now_ts) " << now_ts << endl;
	cout << "Exception message: " << exc.what() << endl;
	// cout << "@" << qinterp.t0 << " : " << qinterp.p0 << endl;
	// cout << "@" << qinterp.t1 << " : " << qinterp.p1 << endl;
	// cout << "Dt : " << qinterp.t1 - qinterp.t0 << endl;
	// cout << "now - t0 : " << now_ts - qinterp.t0 << endl;
	fccout.flush();
	fcaout.flush();
	cliout.flush();               
	exit(1);
      }
      // Update micro tracking and possibly apply rate limitation
      update_micro_tracking(false);
      // Update current target for next mic cycle
      // Moved to fci_update method in fci_update_thread to avoid race condition!:
      // valarr_into_arr(cur_pos, acur_pos);
      if (has_connection && debug_traj) {
	// double tmp = tp_diff(start_tp, now_tp);
	fccout.write((char*)&now_ts, sizeof(double));
	fccout.write((char*)&(cur_pos[4]), sizeof(double));
      }
    }
  }
  
  void fci_interp_micmac(){
    // Generator for next response to the controller.  On an fci
    // update, a new joint position target must be calculated in
    // "acur_pos". If no client is active, a braking or a resting
    // target is synthesized. If an external client is active, a new
    // joint target packet must have been recently received which is
    // propagated. If a client is active, but missed the deadline,
    // extrapolation and slight braking is synthesized. Upon
    // 'deadline_miss_tolerance' missed deadlines, the client is
    // disconnected and full braking is activated.

    bool missed_client_deadline;
    unique_lock<mutex> lck(hot_mtx);

    for(;;){
      // Start by waiting for announced arrival of hot data.
      hot_cond.wait(lck);
      // Yield to let response thread respond.
      // this_thread::yield();
      //this_thread::sleep_for(10us);
      now_ts = pts();
      now_tp = chrono::high_resolution_clock::now();
      if(debug_mic) cout << "fci_update time_step=" <<hot_time_step.toMSec() << "ms" << endl;
      double tmp;
      uint32_t new_micc = mic_cycle + 1; // Wrong, libfranka seems to generate 1 ms calls: + int(hot_time_step.toMSec());
      bool new_macc = new_micc / mics_per_mac;
      mac_cycle += new_micc / mics_per_mac;
      mic_cycle = new_micc % mics_per_mac;
      // cout << "Macc:" << mac_cycle<< " Micc:" << mic_cycle<<endl;
      fms_state = hot_fci_state;
      if(debug_time)
	if (hot_time_step.toSec() > 0.001) cout << "Warning time_step = " << hot_time_step.toSec() << "s" << endl;
      if(debug_mic)
	cout << "Macc:" << mac_cycle << " Micc:" << mic_cycle << endl;
      wrench = (1.0-w_alpha) * wrench + w_alpha * valarray<double>(fms_state.K_F_ext_hat_K.data(),6);
      if(new_macc) {
	// At a macro cycle boundary. We need a new interpolation
	// segment.
	// First send the wrench
	_send_wrench();
	if(debug_mac) cout << "Wrench: " << wrench << endl;
	// Update time stamps
	// prev_prev_ts = prev_ts;
	prev_ts = cur_ts;
	cur_ts = now_ts + mac_cycle_time;
	// prev_prev_tp = prev_tp;
	prev_tp = cur_tp;
	cur_tp = now_tp + chrono::microseconds((unsigned long)(mac_cycle_time*1e6));
	// Check if an external TG is connected
	if (has_connection) {
	  try {
	    // Indicator for missed deadline
	    missed_client_deadline = ! new_connection && ! new_client_packet;
	    // Indicator for first macro cycle for a new connection
	    bool init_macc = new_connection;
	    // Clear flags for new packet and new connection.
	    new_client_packet = false;
	    new_connection = false;
	    // Send the macro state to the external TG.
	    _send_macc_state();
	    if (init_macc) {
	      // Do not act upon a new connection, but continue full braking
	      missed_count = 0;
	      intp_brake(true);
	    }
	    else if (missed_client_deadline) {
	      // We were supposed to have received a packet, but
	      // none was received. Missed deadline or
	      // disconnect. Generate interpolation segment with
	      // mild braking and some attenuation of the
	      // current acceleration.
	      cout << "!!! Warning !!! : no new client packet for control" << endl;
	      missed_count++;
	      if (missed_count > 10){
		cout << "!!! Disconnecting client (missed_count=" << unsigned(missed_count) << ")" << endl;
		connection = base_connection;
		has_connection = false;
		intp_brake(true);
	      }
	      else {
		// Apply mild braking
		intp_brake(false);
	      }
	    }
	    else { // Regular situation where client is connected, and sent a new target pose.
	      // Clear missed count
	      missed_count = 0;
	      // Start the interpolation from the newly commanded joint position.
	      new_tpos = valarray<double>(client_packet.joint_positions_cmd, 7);
	      if (debug_traj) {
		tmp = tp_diff(start_tp, now_tp); // + mac_cycle_time;
		cliout.write((char*)&tmp, sizeof(double));
		tmp = new_tpos[4];
		cliout.write((char*)&tmp, sizeof(double));
		cliout.flush();
	      }
	      intp_client_target();
	    }
	  }
	  catch (std::exception& e) {
	    cout << "Exception : has_connection : " << e.what() << endl;
	  }
	} // end has_connection
	else {
	  // No connection. Generate full braking.
	  if (debug_mic) cout << "No connection, full braking" << endl;
	  intp_brake(true);
	}
      } // end new_macc
      // Shift the position containers and get a new interpolated target position
      prev_pos = cur_pos;
      try {
	//cur_pos = interp(tp_diff(start_tp, now_tp));
	if(quintic){
	  cur_pos = qinterp(mic_cycle_time * mic_cycle);
	}
	else{
	  cur_pos = cinterp(mic_cycle_time * mic_cycle);
	}
      }
      catch (const exception &exc) {
	cout << "Exception when calling interpolator with time (now_ts) " << now_ts << endl;
	cout << "Exception message: " << exc.what() << endl;
	// cout << "@" << qinterp.t0 << " : " << qinterp.p0 << endl;
	// cout << "@" << qinterp.t1 << " : " << qinterp.p1 << endl;
	// cout << "Dt : " << qinterp.t1 - qinterp.t0 << endl;
	// cout << "now - t0 : " << now_ts - qinterp.t0 << endl;
	fccout.flush();
	fcaout.flush();
	cliout.flush();               
	exit(1);
      }

      // Blend to actually commanded position
      //valarray<double> blend_pos = 0.5 * (cur_pos + valarray<double>(fms_state.q_d.data(),7));
      // Pack an std::array over which the create the JointPositions
      //copy(begin(blend_pos), end(blend_pos), begin(acur_pos));
      copy(begin(cur_pos), end(cur_pos), begin(acur_pos));
      if (has_connection && debug_traj) {
	double tmp = tp_diff(start_tp, now_tp);
	fccout.write((char*)&tmp, sizeof(double));
	fccout.write((char*)&(cur_pos[4]), sizeof(double));
      }
      /*      
      // Check velocity continuity
      valarray<double> vq_cmd = (cur_pos - prev_pos)/0.001;
      if (abs(vq_cmd).max() > 2.0){
      if (debug_mac) cout << "Warning: Large joint velocity commanded: "<< vq_cmd << " rad/sec" << endl;
      }
      // Checks acceleration continuity
      valarray<double> aq_cmd = (cur_pos - 2.0*prev_pos + prev_prev_pos)/(1e-6);
      if (abs(aq_cmd).max() > 10.0){
      if (debug_mac) cout << "Warning: Large joint acceleration commanded: "<< aq_cmd << " rad/sec^2" << endl;
      }
      // Check if we are within deadline
      double calc_t = pts() - now_ts;
      if (calc_t > 0.2 * mic_cycle_time) cout << "Warning: Calculation time == " << calc_t << endl;
      if (calc_t > calc_t_max){
      calc_t_max = pts() - now_ts;
      if(debug_time) cout << calc_t_max << endl;
      }
      */
    }
  }
    
  void connection_management(){
    char client_data[max_length];
    Packet new_packet;
    uint8_t i;
    cout << "Connection management starting" << endl;
    for (;;){
      udp::endpoint recv_connection;
      boost::system::error_code error = boost::asio::error::would_block;
      size_t length = fms_sock->receive_from(boost::asio::buffer(client_data, max_length), recv_connection);
      // For non-blocking use:
      // size_t length = fms_sock->receive_from(boost::asio::buffer(client_data, max_length), recv_connection, 0, error);
      if(length == sizeof(Packet)){
        new_packet = *((Packet*)client_data);
        if(debug_mac)
	  cout << "Received client packet of type : " << unsigned(new_packet.type) << endl;
      }
      else{
        cout<<"Warning: Received packet of "<<length<<" bytes. Needed "<<sizeof(Packet)<<endl;
        // Necessary if non-blocking receive is used:
        // this_thread::sleep_for(chrono::microseconds(100));
        continue;
      }
      if(recv_connection == connection){
        // We have received a packet from the registered client
        if(new_packet.type == QUIT){
          has_connection = false;
          cout<<"Disconnect requested : " << connection << endl;
          connection = base_connection;
        }
        else{
          if(new_client_packet){
            cout << "!!! Error !!! : earlier packet was not consumed by control" << endl;
          }
          client_packet = new_packet;
          new_client_packet = true;
        }
      }
      else if (connection == base_connection){
        // We have no registered client. Register the connection if the client says INIT
        if(new_packet.type == INIT) {
          connection = recv_connection;
          new_connection = true;
          cout << "New connection: " << connection << endl;
          Packet ack_packet;
          ack_packet.type = ACK;
          for(auto i=0; i<7; i++){
	    ack_packet.joint_positions_act[i] = fms_state.q[i];
	    ack_packet.joint_positions_cmd[i] = fms_state.q_d[i];
          }
	  if (debug_conn) { cout<<"Sending initial joint values to new client: " << fms_state.q << endl;}
          fms_sock->send_to(boost::asio::buffer((char *)&ack_packet,
						sizeof(Packet)),connection);
          has_connection = true;
        }
        else {
          cout << "New connection was not containing an INIT packet" << endl;
        }
      }
      else{
        Packet rej_state;
        rej_state.type = REJECTED;
        rej_state.cycle = 0;
        cout << "Rejected connection : "<< recv_connection << endl;
        fms_sock->send_to(boost::asio::buffer((char *)&rej_state, sizeof(Packet)), recv_connection);
      }
    }
  }
};
  


#define STACK_SIZE 1024 * 1024 * 1024 // 1 GB

// To be called after mlockall to fix the stack, and before starting the RT-processe
void _alloc_stack() {
  char buffer[STACK_SIZE];
  for (int i=0; i<STACK_SIZE; i++)
    buffer[i] = '\0';
}
      
int main(int argc, char* argv[]) {
  bool emu_fci;
  uint32_t mic_cycle_time_us;
  uint16_t service_port;
  uint32_t mics_per_mac;
  char *fci_host = nullptr;
  start_tp = chrono::high_resolution_clock::now();


  // From https://rt.wiki.kernel.org/index.php/Simple_memory_locking_example
  // Now lock all current and future pages from preventing of being paged
  if (mlockall(MCL_CURRENT | MCL_FUTURE )){
    perror("mlockall failed:");
  }
  // Allocate some stack space
  _alloc_stack();
    
  try {
    if (argc >= 4) {
      // Possibly emulated FCI
      emu_fci = true;
      service_port = atoi(argv[1]);
      mic_cycle_time_us = atoi(argv[2]);
      mics_per_mac = atoi(argv[3]);
      if (argc == 5) {
        // Physical FCI
        emu_fci = false;
        mic_cycle_time_us = 1000;
        fci_host = argv[4];
      }
    }
    else {
      std::cerr << "Usage: franka_motion_service <TG service port (9999)> <emulated micro cycle time [us] (1000)> <mic mac cycle factor (10)> [<Franka controller IP or host>]\n";
      return 1;
    }
    boost::asio::io_service io_service;
    RobWrap robot;
    if (emu_fci) {
      // Emulated:
      robot.init_virt(io_service, mic_cycle_time_us);
    }
    else{
      // Live:
      robot.init_phys(fci_host);
    }
    franka::RobotState initial_rs = robot.readOnce();
    // cout << initial_rs << endl;
    FMS fms(io_service, fci_host, service_port, 1.0e-6 * mic_cycle_time_us, mics_per_mac, initial_rs, emu_fci);
    cout << "FMS thread starting." << endl;
    thread fms_conn_thread(&FMS::connection_management, &fms);
    thread fci_interp_thread(&FMS::fci_interp_rt, &fms);
    thread fci_response_thread(&FMS::fci_response, &fms, std::ref(robot));

    // Set a low nice-value to the process, such that the system level scheduled threads
    // are prioritized at the system level
    nice(-20);
    
    // Give high priority to the FCI-responsible thread, and system
    // level scheduling to the thread handling the external TG
    // connection.
    struct sched_param conn_sch_par, interp_sch_par, resp_sch_par;
    int sch_pol;
    pthread_getschedparam(fms_conn_thread.native_handle(), &sch_pol, &conn_sch_par);
    conn_sch_par.sched_priority = 0;
    pthread_setschedparam(fms_conn_thread.native_handle(), SCHED_OTHER, &conn_sch_par);
    
    pthread_getschedparam(fci_interp_thread.native_handle(), &sch_pol, &interp_sch_par);
    interp_sch_par.sched_priority = 0;
    pthread_setschedparam(fci_interp_thread.native_handle(), SCHED_OTHER, &interp_sch_par);

    pthread_getschedparam(fci_response_thread.native_handle(), &sch_pol, &resp_sch_par);
    resp_sch_par.sched_priority = 99;
    pthread_setschedparam(fci_response_thread.native_handle(), SCHED_FIFO, &resp_sch_par);
    
    fms_conn_thread.join();
    fci_response_thread.join();
    fci_interp_thread.join();
  }
  catch (std::exception& e) {
    std::cerr << "Exception: " << e.what() << endl;
  }
  return 0;
}
